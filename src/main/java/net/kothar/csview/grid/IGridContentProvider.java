package net.kothar.csview.grid;

public interface IGridContentProvider {
	Object getRow(int index);
}
